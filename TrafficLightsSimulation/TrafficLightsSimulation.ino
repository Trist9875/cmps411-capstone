const int trafficPins[3] = {2, 3, 4};
const int SIZE = sizeof(trafficPins)/ sizeof(trafficPins[0]);

int lightLevels[3] = {0, 255, 255};
int pin;
long greenRedInterval = 30000;
long yellowInterval = 4000;
int confirmationBit = 0;


unsigned long previousTime = 0;
 
void setup() {
  // put your setup code here, to run once:
  /* array for pins connected to LEDs */ 
  /* for the future, we need to understand serialization and stream
     so that we can communicate information to the bread board that comes
     from the Machine Learning algorithm that we are to develop.*/
  setLights(trafficPins, lightLevels);
  for(pin = 0; pin < SIZE; pin++)
      pinMode(trafficPins[pin], OUTPUT);

  /*figure the image being sent would be at most 2 megs if uncompressed. 
  We're doing frames, so it may effetively be 30-60x that amount, but we'll see.*/
  Serial.begin(9600);    
  
}

void loop() {
 
    if (lightLevels[2] == 0 && Serial.available()){
       confirmationBit = Serial.read() - '0';
    } else {
      Serial.read();
      }
    
  unsigned long currentTime = millis();
  unsigned long timePassed = currentTime - previousTime;

  /* If the yellow ligh is on and four seconds have elapsed, shift lights and set them to the next step in the cycle.
      PreviousTime is set to the currentTime so that the end/beginning is marked for the next switch in the cycle.    
  */
   if(timePassed >= yellowInterval && lightLevels[1] == 0){
    previousTime = currentTime;
    shiftLights(lightLevels);
    setLights(trafficPins, lightLevels);
    }
    /*If the light is red, there's more than 5 seconds left before it switches, and a vehicle is detected; alert the driver
      of a stopped intersection
      */
    else if(((greenRedInterval - timePassed) > (greenRedInterval * 0.27)) && lightLevels[2] == 0 && confirmationBit == 1){
                  confirmationBit = attentionPattern(trafficPins);
    } 
    /*Otherwise, do as before and set the time for the beginning/end of a switch and shift/set the lights.
    */
    else if(timePassed >= greenRedInterval && (lightLevels[0] == 0|| lightLevels[2] == 0)){
    previousTime = currentTime;
    shiftLights(lightLevels);
    setLights(trafficPins, lightLevels);
    } 
    
  
}

void shiftLights(int* LightArray){
  
  int intermediate = LightArray[2];  
  LightArray[2] = LightArray[1];
  LightArray[1] = LightArray[0];
  LightArray[0] = intermediate;
  }

 int attentionPattern(const int* trafficPins){
  unsigned long startTime = millis();
  unsigned long curTime = millis();  
  int values[2] = {255, 0};
  int i = 0;
 while((curTime - startTime) <= 8000){
  //plan is to have the the light blink 2-3 times then rest for 2 seconds before exiting
  unsigned long passedTime = curTime - startTime;
  unsigned long boundary = 500 * (i + 1);
  if(abs(passedTime - boundary) <= (500*0.1)){
  digitalWrite(trafficPins[2],values[i%2]);
  i++;
  } else if (passedTime >= 6000){
    Serial.read();
    digitalWrite(trafficPins[2], values[2]);
    
    }
  
  curTime = millis();
    }
    
    //make sure that the stream is clean of any data before exiting, and setting the confirmation bit back to one
    return 0;
 } 

void setLights(const int* trafficPins,int* values){
  

     for(int index = 0; index < 3; index++){

      digitalWrite(trafficPins[index], values[index]);
           
      }
  }
