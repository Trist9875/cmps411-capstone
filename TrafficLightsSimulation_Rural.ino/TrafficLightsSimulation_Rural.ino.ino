const int trafficPins[3] = {2, 3, 4};
const int SIZE = sizeof(trafficPins) / sizeof(trafficPins[0]);

int lightSensorPin = A0;       //pin photocell is connected to
int analogValue = 0;
int pattern_counter = 0;
bool pattern_detector = false;
int pattern_input[5] = {0, 0, 0, 0, 0}; // if we're doing the sum piece, modify it to where each spot mimics the increasing powers of
unsigned long startTime = 0;      // two. Gotta do that to avoid an unwanted pattern being recognized as a correct pattern.

int lightLevels[3] = {0, 0, 255};
int pin;
long Intervals[3] = {10000, 30000, 4000};
int confirmationBit = 0;
int detectionBit = 0;
int index = 0;
unsigned long currentTime;


  void shiftLights(int* LightArray) {

    int intermediate = LightArray[2];
    LightArray[2] = LightArray[1];
    LightArray[1] = LightArray[0];
    LightArray[0] = intermediate;
  }

  int attentionPattern(const int* trafficPins) {
    unsigned long startTime = millis();
    unsigned long curTime = millis();
    int values[2] = {255, 0};
    int i = 0;
    while ((curTime - startTime) <= 6000) {
      Serial.read();
      //plan is to have the the light blink 2-3 times then rest for 2 seconds before exiting
      unsigned long passedTime = curTime - startTime;
      unsigned long boundary = 500 * (i + 1);
      if (abs(passedTime - boundary) <= (500 * 0.1)) {
        digitalWrite(trafficPins[2], values[i % 2]);
        i++;
      }
      curTime = millis();
    }

    digitalWrite(trafficPins[2], 0);
    delay(2000);
    //make sure that the stream is clean of any data before exiting, and setting the confirmation bit back to one
    return 0;
  }

  void setLights(const int* trafficPins, int* values) {


    for (int index = 0; index < 3; index++) {

      digitalWrite(trafficPins[index], values[index]);

    }
  }

int validatePattern(int* patternArray) {
  int arraySize = 5;
  int sum = 0;
  

  
  for(int index = 0; index < arraySize; index++){
  int temp = (patternArray[index] == 1) ?  ((int)ceil((pow(2, index)))) : 0;
 
  
  sum += temp;

  }
  if(sum == 13) {

    return 2;
    } else{ 

    
    return 0;
    }

  }

void setup() {
  // put your setup code here, to run once:
  /* array for pins connected to LEDs */
  /* for the future, we need to understand serialization and stream
     so that we can communicate information to the bread board that comes
     from the Machine Learning algorithm that we are to develop.*/
  setLights(trafficPins, lightLevels);
  for (pin = 0; pin < SIZE; pin++)
    pinMode(trafficPins[pin], OUTPUT);

  /*figure the image being sent would be at most 2 megs if uncompressed.
    We're doing frames, so it may effetively be 30-60x that amount, but we'll see.*/
  Serial.begin(9600);
}

void loop() {

  currentTime = millis();
  analogValue = analogRead(lightSensorPin);
  

  //need to simplify later, but should work as long as detectionBit isnt active. which should help with retries.
  if (analogValue >= 100 && !pattern_detector) {  //first value of 75 or greater starts the pattern recognition
    pattern_detector = true;                    //set bool pattern_detector to true while filling the pattern_input array
    pattern_input[pattern_counter] = 1;         //pattern_input array will be filled with 1's for positive light signals
    Serial.println(pattern_input[pattern_counter]);
    //sets startTime to begin the interval checks for the pattern
    startTime = millis();
    pattern_counter++; //hopefully once pattern_detector is true, it'll fall through to this
  } else if (pattern_counter && (currentTime - startTime) >= 1000) {
    //setup like this so the initial starttime state of 0 and the currentTime doesnt accidentially trigger.
    //1000 is 1 second in milliseconds.
   
    if (pattern_counter == 5) {
      //if this goes to the value we've set it as, then it should go to the 1 cycle of lights after this. emphasis on SHOULD
      detectionBit = validatePattern(pattern_input);
      pattern_detector = false;
      pattern_counter = 0;

    } else {
       
      startTime = currentTime;
      pattern_input[pattern_counter] = analogValue > 100 ? 1 : 0;
      Serial.println(analogValue);
      Serial.println(pattern_input[pattern_counter]);
      pattern_counter++;

    }
  }

    while (detectionBit == 2) {
      Serial.read();
      
      unsigned long loopTime = millis();
      //yeah, it's a lil weird, but I reconstituted the previousTime to use in our pattern detection. And, consequently thought renaming it here too was a good idea.
      unsigned long PassedTime = loopTime - currentTime;


       Serial.print("Passed time: ");
       Serial.println(PassedTime);
      if (PassedTime >= Intervals[index % 3]) {
        currentTime = loopTime;

        shiftLights(lightLevels);
        setLights(trafficPins, lightLevels);


        detectionBit = lightLevels[2] == 255 ? 0 : 2 ;
        
        index = lightLevels[2] == 255 ? 0 : ++index ;
      }

    }

    if (Serial.available() && !pattern_detector) {
      confirmationBit = (Serial.read() - '0') == 1 ? 1 : 0;

    }

    if (confirmationBit == 1) {

      confirmationBit = attentionPattern(trafficPins);
    }


  
  }
