from collections import OrderedDict
import math

class Centroid():
    def __init__(self):
        self.nextId = 1
        self.cars = OrderedDict()
        self.lostCars = OrderedDict()
        self.maxLost = 10 #frames
        self.maxDist = 40 #pixels
        
    def addCar(self, centroid, frame):
        self.cars[self.nextId] = centroid
        self.lostCars[self.nextId] = frame
        self.nextId += 1
    
    def removeCar(self, id):
        del self.cars[id]
        del self.lostCars[id]

    def updateCentroid(self, midpoint, frame):
        if len(self.cars) == 0:
            self.addCar(midpoint, frame)
        else:
            found = False
            for i in self.cars:
                dist = math.sqrt(((midpoint[0] - self.cars[i][0]) ** 2) + ((midpoint[1] - self.cars[i][1]) ** 2))
                if dist < self.maxDist:
                    found = True
                    self.cars[i][0] = midpoint[0]
                    self.cars[i][1] = midpoint[1]
                    self.lostCars[i] = frame
                    break
            if found == False:
                self.addCar(midpoint,frame)
            copyCars = self.cars.copy()
            for i in copyCars:
                if((frame - self.lostCars[i]) >= self.maxLost):
                        self.removeCar(i)
        return self.cars