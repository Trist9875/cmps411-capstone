import numpy as np
import cv2
import serial
import time
import centroids
import math

#ser = serial.Serial('/dev/ttyACM0', 9600) #Uncomment this when connected to arduino

n = 0 #Used as a testing pulse to arduino, can be removed
base_time = time.time()

ct = centroids.Centroid()
ct.__init__()

#Taking in the video
capture = cv2.VideoCapture('openCV/OnComing.mp4') #openCV/OnComing.mp4
frame = 0
frame_counter = 0
#xml Cascade to find vehicles (https://github.com/shaanhk/New-GithubTest/blob/master/cars.xml) 
vehicleCascade =  cv2.CascadeClassifier('openCV/cascade.xml')

while True:
    frame += 1
    current_time = time.time()
    ret, frames = capture.read() #reading each frame of video
    frame_counter += 1
    if frame_counter == capture.get(cv2.CAP_PROP_FRAME_COUNT):
        frame_counter = 0 #Or whatever as long as it is the same as next line
        capture.set(cv2.CAP_PROP_POS_FRAMES, 0)
    if frames is None:
        break

# These are the window sizes for all the different frames
# You will have to change these numbers to break the video apart how you want
# Set up as [Y0:Y1,X0:X1]
# (0,0) is top left corner
    full = frames[0:480,0:640] #Full size of camera
    far = frames[0:100,270:410]
    mid = frames[60:180,175:500]
    close = frames[180:360,100:600]

    farGray = cv2.cvtColor(far, cv2.COLOR_BGR2GRAY) #Switching to grayscale
    farcars = vehicleCascade.detectMultiScale(farGray,1.1,1) #setting up detection

    midGray = cv2.cvtColor(mid, cv2.COLOR_BGR2GRAY)
    midcars = vehicleCascade.detectMultiScale(midGray,1.1,1)

    closeGray = cv2.cvtColor(close, cv2.COLOR_BGR2GRAY)
    closecars = vehicleCascade.detectMultiScale(closeGray,1.1,1)

    for(x,y,w,h) in farcars:
        pos = 0
        cv2.rectangle(far,(x,y),(x+w,y+h),(255,0,0),2) #drawing rectangles
        #Uncomment this when connected to arduino, sending 1 to arduino
       
        midPoint = [(x + .5 * w),(y + .5 * h)] #Midpoint and IF statement can be added to the other FOR loops if desired, with tweaking
        
        time_dif = current_time - base_time
        if((midPoint[1] <= 100) and time_dif >= 5): # Checks if the midpoint is above this y value
            base_time = current_time
            #ser.write(b'1')

    for(x,y,w,h) in midcars:
        cv2.rectangle(mid,(x,y),(x+w,y+h),(0,255,0),2)
        midPoint = [(x + .5 * w),(y + .5 * h)] #Midpoint and IF statement can be added to the other FOR loops if desired, with tweaking
        car = ct.updateCentroid(midPoint, frame)
        for i in car:
            cv2.circle(mid,((int)(car[i][0]),(int)(car[i][1])), 2, (0,211,255), 2)
            cv2.putText(mid, str(i),((int)(car[i][0]-5),(int)(car[i][1]-5)), cv2.FONT_HERSHEY_SIMPLEX, .6, (0,211,255), 1) 
            
            
    for(x,y,w,h) in closecars:
        cv2.rectangle(close,(x,y),(x+w,y+h),(0,0,255),2)

    cv2.line(far,(0,100),(600,100),(255,255,255),2) #Used to visualize the line midpoint is using

    #cv2.imshow('OnComingFar', far) #showing video with rectangles
    cv2.imshow('OnComingMiddle', mid)
    #cv2.imshow('OnComingClose', close)
    cv2.imshow('Full',full)
    
    if cv2.waitKey(33) == 27: #waitng for "esc" to close windows
        break
    
cv2.destroyAllWindows() #closes windows after "esc" or end of videos